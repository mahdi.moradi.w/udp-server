#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <stdlib.h>
#include <signal.h>

void intHandler(int dummy) {
    _exit(0);
}

#define IP_PROTOCOL 0
#define BUFFER_SIZE 4096
#define FLAG 0
#define FILE_NOT_FOUND_MSG "File Not Found!"

int main(int argc, char **argv) {

    int socketFD;
    int interface_FD;
    int port_number;
    char * interface_name;
    char * port_number_ch;
    struct ifreq ifr;
    struct sockaddr_in server_addr,client_addr;
    unsigned char buffer[BUFFER_SIZE];
    unsigned long big_endian = 0;
    unsigned long file_size = 0;
    FILE *fp;

    struct sigaction act;
    act.sa_handler = intHandler;
    act.sa_flags = 0;
    sigaction(SIGINT, &act, NULL);



    if(argv[1] == NULL){
        printf("no interface declared\n");
        exit(EXIT_FAILURE);
    }

    if(argv[2] == NULL){
        printf("no port number declared\n");
        exit(EXIT_FAILURE);
    }


    interface_name = argv[1];
    port_number_ch = argv[2];
    port_number = atoi(port_number_ch);

    interface_FD = socket(AF_INET, SOCK_DGRAM, IP_PROTOCOL);
    ifr.ifr_addr.sa_family = AF_INET;
    strncpy(ifr.ifr_name, interface_name, IFNAMSIZ-1);
    ioctl(interface_FD, SIOCGIFADDR, &ifr);
    close(interface_FD);

    int server_addr_len = sizeof(server_addr);
    int client_addr_len = sizeof(client_addr);

    memset(&server_addr,0,server_addr_len);
    memset(&server_addr,0,server_addr_len);

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(port_number);
    server_addr.sin_addr = ((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr;

    socketFD = socket(AF_INET, SOCK_DGRAM, IP_PROTOCOL);
    if (socketFD < 0) {
        perror("file descriptor not received!!\n");
    }
    else {
        printf("file descriptor %d received [ press ctrl-c to stop the server ]\n", socketFD);

        if (bind(socketFD, (struct sockaddr *) &server_addr, sizeof(server_addr)) == 0)
            printf("Successfully binded!\n");
        else {
            perror("Binding Failed!\n");
            exit(EXIT_FAILURE);
        }

        printf("server started : \n");
        printf("\tinterface : [%s] \n",interface_name);
        printf("\tip : [%s] \n",inet_ntoa(server_addr.sin_addr));
        printf("\tport : [%d] \n",port_number);

        while (1) {
            printf("-----------------------------\n");
            printf("Waiting for file name...\n");
            memset(buffer, '\0', BUFFER_SIZE);

            recvfrom(socketFD, buffer, BUFFER_SIZE, FLAG, (struct sockaddr *) &client_addr, &client_addr_len);
            printf("\tclient ip : [%s] \n", inet_ntoa(client_addr.sin_addr));

            fp = fopen(buffer, "rb");

            printf("\tFile Name Received: %s\n", buffer);
            if (fp == NULL) {
                printf("File open failed!\n");
                sendto(socketFD, &file_size, sizeof(file_size), FLAG, (struct sockaddr *) &client_addr, client_addr_len);
                sendto(socketFD, FILE_NOT_FOUND_MSG, sizeof(FILE_NOT_FOUND_MSG), FLAG, (struct sockaddr *) &client_addr, client_addr_len);
                exit(1);
            }
            else {
                printf("\tFile Successfully opened!\n");

                fseek(fp, 0, SEEK_END);
                file_size = ftell(fp);
                fseek(fp, 0, SEEK_SET);

                printf("\tFile Size: %lu\n", file_size);

                big_endian = htonl(file_size);
                sendto(socketFD, &big_endian, sizeof(big_endian), FLAG, (struct sockaddr *) &client_addr,client_addr_len);

                int iResult = 0;
                do {
                    iResult += fread(buffer, sizeof(char), BUFFER_SIZE, fp);
                    sendto(socketFD, buffer, sizeof(buffer), FLAG, (struct sockaddr *) &client_addr, client_addr_len);
                    recvfrom(socketFD, buffer, BUFFER_SIZE, FLAG, (struct sockaddr *) &client_addr, &client_addr_len);
                    if (strstr(buffer, "ACK") == NULL) {
                        printf("error in sending file ...");
                        exit(EXIT_FAILURE);
                    }
                } while (iResult != file_size);

                fclose(fp);
                printf("File sent\n");

            }
        }
        return 0;
    }
}
